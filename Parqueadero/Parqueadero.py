#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cola import Cola

class Parqueadero(object):
	def __init__(self):
		self.Parqueos = Cola()

	def inscribir(self, est):
		self.Parqueos.encolar(est)

	def atender(self):
		return self.Parqueos.desencolar()

	def cantidad(self):
		return self.Parqueos.cantidad()