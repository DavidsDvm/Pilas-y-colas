# -*- coding: utf-8 -*-
from copy import deepcopy

# Ejercicio Peliculas en el que una persona posee una colección
# de peliculas y en dado momento desea buscar una en especifico.

import pelicula
import pila

# Colección de peliculas.
coleccionPelicula = pila.Pila()

def agregarPelicula(pelicula):
    """ Agrega un nueva pelicula a la colección. """
    coleccionPelicula.apilar(pelicula)

def listarPeliculas(coleccion):
    """ Muestra la descripción de las peliculas de una colección. """
    y = deepcopy (coleccion)
    while y.es_vacia() == False:
        x = y.desapilar()
        print("Pelicula "+x.nombre+" [ Año: "+str(x.año)+
              " - Genero:"+x.genero+" - Actor: "+x.actor+" ]")
    print("///////////////////////////////////////////////////////////////////////////////")

def buscarPelicula(coleccion, dato):
    """ Busca una pelicula en especifico. """
    coleccionAuxiliar = pila.Pila()
    encontrado = False
    while coleccionPelicula.es_vacia() == False:
        x = coleccionPelicula.desapilar()
        coleccionAuxiliar.apilar(x)
        if(dato == x.nombre or dato == x.año or dato == x.genero or dato == x.actor):
            print("------------------------  COLECCIÓN AUXILIAR PELICULAS:    -----------------")
            print("PELICULA ENCONTRADA :D ")
            listarPeliculas(coleccionAuxiliar)
            encontrado = True
            break
    if encontrado == False:
        print("PELICULA NO ENCONTRADA :( ")
        listarPeliculas(coleccionAuxiliar)
    
    
# Se crean algunas peliculas.
pelChuky = pelicula.Pelicula("Chuky", 1988, "Terror", "Brad Dourif")
pelElConjuro = pelicula.Pelicula("El Conjuro", 2013, "Terror", "Vera Farmiga")
pelRocky = pelicula.Pelicula("Rocky", 1976, "Drama", "Sylvester Stallone")
pelTitanic = pelicula.Pelicula("Titanic", 1997, "Romance", "Leonardo DiCaprio")
pelTransformers = pelicula.Pelicula("Transformers", 2013, "Ciencia Ficción", "Shia LaBeouf")
pelPulpFiction = pelicula.Pelicula("Pulp Fiction", 1994, "Thriller", "John Travolta")

# Se agregan las peliculas a la colección.
coleccionPelicula.apilar(pelChuky)
coleccionPelicula.apilar(pelElConjuro)
coleccionPelicula.apilar(pelRocky)
coleccionPelicula.apilar(pelTitanic)
coleccionPelicula.apilar(pelTransformers)
coleccionPelicula.apilar(pelPulpFiction)

# Listamos las peliculas
print("------------------------  COLECCIÓN PELICULAS:    --------------------------")
listarPeliculas(coleccionPelicula)
# Buscamos una pelicula llamada Titanic
buscarPelicula(coleccionPelicula,"Titanic")